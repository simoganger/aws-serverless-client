window._config = {
  cognito: {
    userPoolId: "us-east-1_fjkQ6j8B3", // e.g. us-east-2_uXboG5pAb
    userPoolClientId: "tl0md0nfd54pj6c135b61b78o", // e.g. 25ddkmj4v6hfsfvruhpfi7n4hv
    region: "us-east-1", // e.g. us-east-2
  },
  api: {
    invokeUrl: "https://6b86wx0i72.execute-api.us-east-1.amazonaws.com/prod", // e.g. https://rc7nyt4tql.execute-api.us-west-2.amazonaws.com/prod',
  },
};
